#!/bin/bash

# make the file if it doesn't exist
if [ ! -f $CONF_DIR/.tmux.conf_l ]; then
	touch $CONF_DIR/.tmux.conf_l
fi


python $PKG_DIR/conf-tmux/theme.py
